namespace entities;

public class Jobs
{
    public int Id { get; set; }
    public int JobTypeCatalogsId { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int StatusCatalogsId { get; set; }
    public DateTime DateAdded { get; set; }
    public DateTime DateUpdated { get; set; }


    /// References
    public virtual Catalogs JobTypeCatalogs { get; set; }
    public virtual Catalogs StatusCatalogs { get; set; }


}