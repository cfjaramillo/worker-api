namespace entities;
public class Email
{
    public string EmailTo { get; set; }
    public string NameTo { get; set; }
    public string Subject { get; set; }
    public string PlainContent { get; set; }

}