namespace entities;


/// <summary>
/// Class <c>Catalog</c> models application catalogs.
/// </summary>
public class Catalogs
{
    public int Id { get; set; }
    public int CatalogsTypeId { get; set; }
    public string Name { get; set; }
    public string Value { get; set; }
    public DateTime DateAdded { get; set; }
    public DateTime DateUpdated { get; set; }

    /// References
    public virtual CatalogsType CatalogsType { get; set; }
    public virtual ICollection<Jobs> JobsTypes { get; set; }
    public virtual ICollection<Jobs> JobsStatus { get; set; }

}

