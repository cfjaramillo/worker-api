namespace entities;

/// <summary>
/// Class <c>CatalogsType</c> models application type catalogs.
/// </summary>
public class CatalogsType
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime DateAdded { get; set; }
    public DateTime DateUpdated { get; set; }

    /// References
    public virtual ICollection<Catalogs> Catalogs { get; set; }

}