using api.Domain.Commands.Interfaces;
using api.Infra.UnitOfWork;
using entities;

namespace api.Domain.Commands;

public class JobCommands : IJobCommands
{
    private readonly IUnitOfWork _unitOfWork;

    public JobCommands(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<bool> CreateAsync(Jobs job)
    {
        _unitOfWork.jobRepository.Insert(job);
        return await _unitOfWork.CompleteAsync();
    }

    public async Task<bool> DeleteAsync(int id)
    {
        _unitOfWork.jobRepository.Delete(id);
        return await _unitOfWork.CompleteAsync();
    }

    public async Task<bool> UpdateAsync(Jobs job)
    {
        var entity = await _unitOfWork.jobRepository.GetByIdAsync(job.Id);

        entity.Description = job.Description;
        entity.Name = job.Name;
        entity.JobTypeCatalogsId = job.JobTypeCatalogsId;


        _unitOfWork.jobRepository.Update(entity);
        return await _unitOfWork.CompleteAsync();
    }
}