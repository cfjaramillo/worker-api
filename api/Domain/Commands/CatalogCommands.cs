using api.Domain.Commands.Interfaces;
using api.Infra.UnitOfWork;
using entities;

namespace api.Domain.Commands;

public class CatalogCommands : ICatalogCommands
{
    private readonly IUnitOfWork _unitOfWork;

    public CatalogCommands(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<bool> CreateAsync(Catalogs catalog)
    {
        _unitOfWork.catalogRepository.Insert(catalog);
        return await _unitOfWork.CompleteAsync();
    }

    public async Task<bool> DeleteAsync(int id)
    {
        _unitOfWork.catalogRepository.Delete(id);
        return await _unitOfWork.CompleteAsync();
    }

    public async Task<bool> UpdateAsync(Catalogs catalog)
    {
        var entity = await _unitOfWork.catalogRepository.GetByIdAsync(catalog.Id);

        entity.CatalogsTypeId = catalog.CatalogsTypeId;
        entity.Name = catalog.Name;
        entity.Value = catalog.Value;


        _unitOfWork.catalogRepository.Update(entity);
        return await _unitOfWork.CompleteAsync();
    }
}