using api.Domain.Commands.Interfaces;
using api.Infra.UnitOfWork;
using entities;

namespace api.Domain.Commands;

public class UserCommands : IUserCommands
{
    private readonly IUnitOfWork _unitOfWork;

    public UserCommands(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<bool> CreateAsync(Users user)
    {
        _unitOfWork.userRepository.Insert(user);
        return await _unitOfWork.CompleteAsync();
    }

    public async Task<bool> DeleteAsync(int id)
    {
        _unitOfWork.userRepository.Delete(id);
        return await _unitOfWork.CompleteAsync();
    }

    public async Task<bool> UpdateAsync(Users user)
    {
        var entity = await _unitOfWork.userRepository.GetByIdAsync(user.Id);

        entity.Name = user.Name;
        entity.Phone = user.Phone;


        _unitOfWork.userRepository.Update(entity);
        return await _unitOfWork.CompleteAsync();
    }
}