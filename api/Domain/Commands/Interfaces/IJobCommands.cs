using entities;

namespace api.Domain.Commands.Interfaces;

public interface IJobCommands
{
    Task<bool> CreateAsync(Jobs job);
    Task<bool> UpdateAsync(Jobs job);
    Task<bool> DeleteAsync(int id);
    
}