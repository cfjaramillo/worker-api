using entities;

namespace api.Domain.Commands.Interfaces;

public interface IUserCommands
{
    Task<bool> CreateAsync(Users user);
    Task<bool> UpdateAsync(Users user);
    Task<bool> DeleteAsync(int id);
    
}