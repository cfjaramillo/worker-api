using entities;

namespace api.Domain.Commands.Interfaces;

public interface ICatalogCommands
{
    Task<bool> CreateAsync(Catalogs catalog);
    Task<bool> UpdateAsync(Catalogs catalog);
    Task<bool> DeleteAsync(int id);
    
}