using api.Domain.Queries.Interfaces;
using entities;
using api.Infra.UnitOfWork;

namespace api.Domain.Queries;

public class UserQueries : IUserQueries
{
    private readonly IUnitOfWork _unitOfWork;

    public UserQueries(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<IEnumerable<Users>> GetUsersAsync()
    {
        return await _unitOfWork.userRepository.GetAllAsync();
    }

    public async Task<Users> GetUsersByIdAsync(int id)
    {
        return await _unitOfWork.userRepository.GetByIdAsync(id);
    }
}