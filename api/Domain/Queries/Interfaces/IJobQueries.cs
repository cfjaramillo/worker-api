using entities;

namespace api.Domain.Queries.Interfaces;

public interface IJobQueries
{
    Task<IEnumerable<Jobs>> GetJobsAsync();
    Task<Jobs> GetJobsByIdAsync(int id);
    Task<IEnumerable<Jobs>> GetJobsByExpression(Func<Jobs, bool> lambda);

}