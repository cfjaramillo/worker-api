using entities;

namespace api.Domain.Queries.Interfaces;

public interface ICatalogQueries
{
    Task<IEnumerable<Catalogs>> GetCatalogsAsync();
    Task<Catalogs> GetCatalogsByIdAsync(int id);
    Task<IEnumerable<Catalogs>> GetCatalogsByCatalogTypeIdAsync(int idCatalogType);

}