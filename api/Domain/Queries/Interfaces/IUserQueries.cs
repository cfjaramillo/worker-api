using entities;

namespace api.Domain.Queries.Interfaces;

public interface IUserQueries
{
    Task<IEnumerable<Users>> GetUsersAsync();
    Task<Users> GetUsersByIdAsync(int id);

}