using api.Domain.Queries.Interfaces;
using entities;
using api.Infra.UnitOfWork;

namespace api.Domain.Queries;

public class CatalogQueries : ICatalogQueries
{
    private readonly IUnitOfWork _unitOfWork;

    public CatalogQueries(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<IEnumerable<Catalogs>> GetCatalogsAsync()
    {
        return await _unitOfWork.catalogRepository.GetAllAsync();
    }

    public async Task<Catalogs> GetCatalogsByIdAsync(int id)
    {
        return await _unitOfWork.catalogRepository.GetByIdAsync(id);
    }

    public async Task<IEnumerable<Catalogs>> GetCatalogsByCatalogTypeIdAsync(int idCatalogType)
    {
        var result = await _unitOfWork.catalogRepository.GetCatalogsByCatalogTypeIdAsync(idCatalogType);
        return result;
    }
}