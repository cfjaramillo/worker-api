using api.Domain.Queries.Interfaces;
using entities;
using api.Infra.UnitOfWork;

namespace api.Domain.Queries;

public class JobQueries : IJobQueries
{
    private readonly IUnitOfWork _unitOfWork;

    public JobQueries(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<IEnumerable<Jobs>> GetJobsAsync()
    {
        return await _unitOfWork.jobRepository.GetAllAsync();
    }

    public async Task<Jobs> GetJobsByIdAsync(int id)
    {
        return await _unitOfWork.jobRepository.GetByIdAsync(id);
    }

    public async Task<IEnumerable<Jobs>> GetJobsByExpression(Func<Jobs, bool> lambda)
    {
        return await _unitOfWork.jobRepository.GetByExpression(lambda);
    }
}