using entities;

namespace api.Services.Interfaces;

public interface ISendGridServiceClient
{
    Task<bool> SendEmailAsync(Email email);
}