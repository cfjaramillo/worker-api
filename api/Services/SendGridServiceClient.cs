using api.Services.Interfaces;
using entities;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace api.Services;

public class SendGridServiceClient : ISendGridServiceClient
{
    private readonly SendGridClient _sendGridClient;
    private readonly string _fromEmail;
    private readonly string _fromName;

    public SendGridServiceClient(IConfiguration configuration)
    {
        this._sendGridClient = new SendGridClient(configuration["SendGrid:ApiKey"].ToString());
        this._fromEmail = configuration["SendGrid:FromEmail"];
        this._fromName = configuration["SendGrid:FromName"];
    }

    public async Task<bool> SendEmailAsync(Email email)
    {
        var msg = new SendGridMessage
        {
            From = new EmailAddress(_fromEmail, _fromName),
            Subject = email.Subject,
            PlainTextContent = email.PlainContent
        };

        msg.AddTo(email.EmailTo, email.NameTo);
        var response = await _sendGridClient.SendEmailAsync(msg);
        return response.IsSuccessStatusCode;
    }
}