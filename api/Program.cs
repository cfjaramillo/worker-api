using System.Text.Json.Serialization;
using api.Domain.Commands;
using api.Domain.Commands.Interfaces;
using api.Domain.Queries;
using api.Domain.Queries.Interfaces;
using api.Infra;
using api.Infra.UnitOfWork;
using api.Services;
using api.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Web;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration["Mysql:ConnectionString"].ToString();

// Add services to the container.
builder.Services.AddTransient<ICatalogCommands, CatalogCommands>();
builder.Services.AddTransient<ICatalogQueries, CatalogQueries>();
builder.Services.AddTransient<IJobCommands, JobCommands>();
builder.Services.AddTransient<IJobQueries, JobQueries>();
builder.Services.AddTransient<IUserCommands, UserCommands>();
builder.Services.AddTransient<IUserQueries, UserQueries>();
builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();
builder.Services.AddTransient<ISendGridServiceClient, SendGridServiceClient>();
// DbContext
builder.Services.AddDbContext<MysqlDbContext>(
    options => options.UseMySql(connectionString,ServerVersion.AutoDetect(connectionString))
);

builder.Services.AddControllers().AddJsonOptions(o => o.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// AZURE AD AUTH
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddMicrosoftIdentityWebApi(builder.Configuration);
builder.Services.AddAuthorization();

// CORS
builder.Services.AddCors(
    options => options.AddPolicy(
        "local",
        builder =>
        {
            builder.AllowAnyOrigin();
            builder.AllowAnyHeader();
            builder.AllowAnyMethod();
        }
    )
);

var app = builder.Build();

// Apply Migrations
using (var scope = app.Services.CreateScope())
{
    var mySqlDbContext = scope.ServiceProvider.GetRequiredService<MysqlDbContext>();
    mySqlDbContext.Database.Migrate();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
// CORS
app.UseCors("local");
// AZURE AD AUTH
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
