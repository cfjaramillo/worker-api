﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace api.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            InsertCatalogsType(migrationBuilder);
            InsertCatalogs(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }

        protected void InsertCatalogsType(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT INTO `CatalogsType` (`Name`) VALUES ('IdentificationTypes');");
            migrationBuilder.Sql(@"INSERT INTO `CatalogsType` (`Name`) VALUES ('WorkCategories');");
            migrationBuilder.Sql(@"INSERT INTO `CatalogsType` (`Name`) VALUES ('StatusWork');");
            migrationBuilder.Sql(@"INSERT INTO `CatalogsType` (`Name`) VALUES ('PaymentPeriod');");
        }

        protected void InsertCatalogs(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('1', 'Cedula de Ciudadania', 'CC');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('1', 'Cedula de Extranjeria', 'CE');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('2', 'FINANCE', 'FINANCE');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('2', 'AGRICULTURE', 'AGRICULTURE');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('2', 'MARKETING', 'MARKETING');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('2', 'IT', 'IT');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('2', 'BANKING', 'BANKING');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('2', 'HEALTHCARE', 'HEALTHCARE');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('2', 'GOVERNMENT', 'GOVERNMENT');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('2', 'FOOD', 'FOOD');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('2', 'ENTERTAINMENT', 'ENTERTAINMENT');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('3', 'CREATED', '13');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('3', 'PROGRESS', '14');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('3', 'STOPPED', '15');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('3', 'CANCELED', '16');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('4', 'weekly', '7');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('4', 'bi-weekly', '15');");
            migrationBuilder.Sql(@"INSERT INTO `Catalogs` (`CatalogsTypeId`, `Name`, `Value`) VALUES ('4', 'monthly', '30');");
        }
    }
}
