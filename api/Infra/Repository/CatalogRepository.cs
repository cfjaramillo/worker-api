using api.Infra.Repository.Interfaces;
using entities;
using Microsoft.EntityFrameworkCore;

namespace api.Infra.Repository;

public class CatalogRepository : BaseRepository<Catalogs>, ICatalogRepository
{
    public CatalogRepository(MysqlDbContext context) : base(context)
    {

    }

    public async Task<IEnumerable<Catalogs>> GetCatalogsByCatalogTypeIdAsync(int idCatalogType)
    {
        return await _dbSet.Where(c => c.CatalogsTypeId.Equals(idCatalogType)).ToListAsync();
    }

}