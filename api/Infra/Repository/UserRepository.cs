using api.Infra.Repository.Interfaces;
using entities;

namespace api.Infra.Repository;

public class UserRepository : BaseRepository<Users>, IUserRepository
{
    public UserRepository(MysqlDbContext context) : base(context)
    {
        
    }

}