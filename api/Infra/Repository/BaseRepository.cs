using api.Infra.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Infra.Repository;

public class BaseRepository<T> : IBaseRepository<T> where T : class
{
    internal MysqlDbContext _context;
    internal DbSet<T> _dbSet;

    public BaseRepository(MysqlDbContext context)
    {
        this._context = context;
        this._dbSet = context.Set<T>();
    }

    public void Delete(int id)
    {
        var entity = _context.Set<T>().Find(id);
        _context.Remove(entity);
    }

    public async Task<IEnumerable<T>> GetAllAsync()
    {
        return await _context.Set<T>().ToListAsync();
    }

    public async Task<T> GetByIdAsync(int id)
    {
        return await _context.Set<T>().FindAsync(id);
    }

    public async Task<IEnumerable<T>> GetByExpression(Func<T, bool> lambda)
    {
        return await Task.FromResult(_context.Set<T>().Where(lambda).ToList());
    }

    public void Insert(T obj)
    {
        _context.Set<T>().Add(obj);
    }

    public void Update(T obj)
    {
        _context.Set<T>().Update(obj);
    }
}