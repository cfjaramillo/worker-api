using entities;

namespace api.Infra.Repository.Interfaces;
public interface ICatalogRepository : IBaseRepository<Catalogs>
{
    Task<IEnumerable<Catalogs>> GetCatalogsByCatalogTypeIdAsync(int idCatalogType);
}