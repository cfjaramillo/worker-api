namespace api.Infra.Repository.Interfaces;

public interface IBaseRepository<T> where T : class
{
    Task<IEnumerable<T>> GetAllAsync();
    Task<T> GetByIdAsync(int id);
    Task<IEnumerable<T>> GetByExpression(Func<T, bool> lambda);
    void Insert(T obj);
    void Update(T obj);
    void Delete(int id);
}