using api.Infra.Repository.Interfaces;
using entities;

namespace api.Infra.Repository;

public class JobRepository : BaseRepository<Jobs>, IJobRepository
{
    public JobRepository(MysqlDbContext context) : base(context)
    {
        
    }

}