using Microsoft.EntityFrameworkCore;
using entities;

namespace api.Infra;


/// <summary>
/// Class <c>MysqlDbContext</c> models application dbcontext.
/// </summary>
public class MysqlDbContext : DbContext
{
    public MysqlDbContext(DbContextOptions options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // modelBuilder.HasDefaultSchema("Worker");
        base.OnModelCreating(modelBuilder);
        PrimaryKeysColumns(modelBuilder);
        RequiredColumns(modelBuilder);
        DefaultValueColumns(modelBuilder);
        ForeignKeys(modelBuilder);
        NavigatorFields(modelBuilder);
    }

    private static void PrimaryKeysColumns(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Catalogs>().HasKey(k => k.Id);
        modelBuilder.Entity<CatalogsType>().HasKey(k => k.Id);
        modelBuilder.Entity<Users>().HasKey(k => k.Id);
        modelBuilder.Entity<Jobs>().HasKey(k => k.Id);
    }

    private static void RequiredColumns(ModelBuilder modelBuilder)
    {
        /// Catalogs
        modelBuilder.Entity<Catalogs>().Property(p => p.Name).IsRequired();
        modelBuilder.Entity<Catalogs>().Property(p => p.Value).IsRequired();

        /// Catalogs Type
        modelBuilder.Entity<CatalogsType>().Property(p => p.Name).IsRequired();

        /// Users
        modelBuilder.Entity<Users>().Property(p => p.Name).IsRequired();
        modelBuilder.Entity<Users>().Property(p => p.Phone).IsRequired();
        modelBuilder.Entity<Users>().Property(p => p.Email).IsRequired();
        modelBuilder.Entity<Users>().Property(p => p.TermsAccepted).IsRequired();

        /// Jobs
        modelBuilder.Entity<Jobs>().Property(p => p.Name).IsRequired();
        modelBuilder.Entity<Jobs>().Property(p => p.Description).IsRequired();
    }

    private static void DefaultValueColumns(ModelBuilder modelBuilder)
    {
        // /// Catalogs
        // modelBuilder.Entity<Catalogs>().Property(p => p.DateAdded).HasDefaultValueSql("CURRENT_TIMESTAMP ");
        // modelBuilder.Entity<Catalogs>().Property(p => p.DateUpdated).HasDefaultValueSql("CURRENT_TIMESTAMP ");

        // /// Catalogs Type
        // modelBuilder.Entity<CatalogsType>().Property(p => p.DateAdded).HasDefaultValueSql("CURRENT_TIMESTAMP ");
        // modelBuilder.Entity<CatalogsType>().Property(p => p.DateUpdated).HasDefaultValueSql("CURRENT_TIMESTAMP ");

        // /// Jobs
        // modelBuilder.Entity<Jobs>().Property(p => p.DateAdded).HasDefaultValueSql("CURRENT_TIMESTAMP ");
        // modelBuilder.Entity<Jobs>().Property(p => p.DateUpdated).HasDefaultValueSql("CURRENT_TIMESTAMP ");
    }

    private static void ForeignKeys(ModelBuilder modelBuilder)
    {
        /// Catalogs
        modelBuilder.Entity<Catalogs>()
            .HasOne<CatalogsType>(s => s.CatalogsType)
            .WithMany(g => g.Catalogs)
            .HasForeignKey(s => s.CatalogsTypeId);

        /// Jobs
        modelBuilder.Entity<Jobs>()
            .HasOne<Catalogs>(s => s.JobTypeCatalogs)
            .WithMany(g => g.JobsTypes)
            .HasForeignKey(s => s.JobTypeCatalogsId);

        modelBuilder.Entity<Jobs>()
            .HasOne<Catalogs>(s => s.StatusCatalogs)
            .WithMany(g => g.JobsStatus)
            .HasForeignKey(s => s.StatusCatalogsId);
    }

    private static void NavigatorFields(ModelBuilder modelBuilder)
    {
        /// Catalogs
        modelBuilder.Entity<Catalogs>().Navigation(c => c.CatalogsType).AutoInclude();
        /// Jobs
        modelBuilder.Entity<Jobs>().Navigation(j => j.JobTypeCatalogs).AutoInclude();
        modelBuilder.Entity<Jobs>().Navigation(j => j.StatusCatalogs).AutoInclude();

    }


    /// Tables
    public DbSet<Catalogs> Catalogs;
    public DbSet<CatalogsType> CatalogsTypes;
    public DbSet<Users> Users;
    public DbSet<Jobs> Jobs;


}