using System.Threading.Tasks;
using api.Infra.Repository.Interfaces;

namespace api.Infra.UnitOfWork;

public interface IUnitOfWork
{
    ICatalogRepository catalogRepository { get; }
    IJobRepository jobRepository { get; }
    IUserRepository userRepository { get; }
    Task<bool> CompleteAsync();

}