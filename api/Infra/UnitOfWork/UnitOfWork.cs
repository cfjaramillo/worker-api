using System.Threading.Tasks;
using api.Infra.Repository;
using api.Infra.Repository.Interfaces;

namespace api.Infra.UnitOfWork;

public class UnitOfWork : IUnitOfWork
{
    private readonly MysqlDbContext _context;

    public ICatalogRepository catalogRepository { get; private set; }
    public IJobRepository jobRepository { get; private set; }
    public IUserRepository userRepository { get; private set; }

    public UnitOfWork(MysqlDbContext context)
    {
        _context = context;
        catalogRepository = new CatalogRepository(context);
        jobRepository = new JobRepository(context);
        userRepository = new UserRepository(context);
    }

    public async Task<bool> CompleteAsync()
    {
        await _context.SaveChangesAsync();
        return true;
    }
}