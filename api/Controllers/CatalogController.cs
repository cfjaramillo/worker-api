using api.Domain.Commands.Interfaces;
using api.Domain.Queries.Interfaces;
using entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class CatalogController : ControllerBase
{
    private readonly ICatalogCommands _catalogCommands;
    private readonly ICatalogQueries _catalogQueries;

    private readonly ILogger<WorkerController> _logger;

    public CatalogController(ILogger<WorkerController> logger, ICatalogCommands catalogCommands, ICatalogQueries catalogQueries)
    {
        this._catalogCommands = catalogCommands;
        this._catalogQueries = catalogQueries;
        this._logger = logger;
    }

    [HttpGet]
    public async Task<IEnumerable<Catalogs>> GetAsync()
    {
        var result = await _catalogQueries.GetCatalogsAsync();
        return result;
    }

    [HttpGet("{catalogId:int}")]
    public async Task<Catalogs> GetByIdAsync([FromRoute] int catalogId)
    {
        var result = await _catalogQueries.GetCatalogsByIdAsync(catalogId);
        return result;
    }

    [HttpGet("catalogtype/{catalogTypeId:int}")]
    public async Task<IEnumerable<Catalogs>> GetCatalogsByCatalogTypeAsync([FromRoute] int catalogTypeId)
    {
        var result = await _catalogQueries.GetCatalogsByCatalogTypeIdAsync(catalogTypeId);
        return result;
    }

    [HttpPost]
    public async Task<bool> CreateAsync([FromBody] Catalogs catalog)
    {
        var result = await _catalogCommands.CreateAsync(catalog);
        return result;
    }

    [HttpPut]
    public async Task<bool> UpdateAsync([FromBody] Catalogs catalog)
    {
        var result = await _catalogCommands.UpdateAsync(catalog);
        return result;
    }

    [HttpDelete("{catalogId:int}")]
    public async Task<bool> DeleteAsync([FromRoute] int catalogId)
    {
        var result = await _catalogCommands.DeleteAsync(catalogId);
        return result;
    }
}
