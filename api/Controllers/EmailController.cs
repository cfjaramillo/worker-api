using api.Services.Interfaces;
using entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class EmailController : ControllerBase
{
    private readonly ISendGridServiceClient _emailServiceClient;

    private readonly ILogger<WorkerController> _logger;

    public EmailController(ILogger<WorkerController> logger, ISendGridServiceClient emailServiceClient)
    {
        this._emailServiceClient = emailServiceClient;
        this._logger = logger;
    }

    [HttpPost]
    public async Task<bool> SendAsync([FromBody] Email email)
    {
        var result = await _emailServiceClient.SendEmailAsync(email);
        return result;
    }
}
