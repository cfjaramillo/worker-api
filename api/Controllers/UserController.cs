using api.Domain.Commands.Interfaces;
using api.Domain.Queries.Interfaces;
using entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly IUserCommands _userCommands;
    private readonly IUserQueries _userQueries;

    private readonly ILogger<WorkerController> _logger;

    public UserController(ILogger<WorkerController> logger, IUserCommands userCommands, IUserQueries userQueries)
    {
        this._userCommands = userCommands;
        this._userQueries = userQueries;
        this._logger = logger;
    }

    [HttpGet]
    public async Task<IEnumerable<Users>> GetAsync()
    {
        var result = await _userQueries.GetUsersAsync();
        return result;
    }

    [HttpGet("{userId:int}")]
    public async Task<Users> GetByIdAsync([FromRoute] int userId)
    {
        var result = await _userQueries.GetUsersByIdAsync(userId);
        return result;
    }

    [HttpPost]
    public async Task<bool> CreateAsync([FromBody] Users user)
    {
        var result = await _userCommands.CreateAsync(user);
        return result;
    }

    [HttpPut]
    public async Task<bool> UpdateAsync([FromBody] Users user)
    {
        var result = await _userCommands.UpdateAsync(user);
        return result;
    }

    [HttpDelete("{userId:int}")]
    public async Task<bool> DeleteAsync([FromRoute] int userId)
    {
        var result = await _userCommands.DeleteAsync(userId);
        return result;
    }
}
