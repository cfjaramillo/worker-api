using api.Domain.Commands.Interfaces;
using api.Domain.Queries.Interfaces;
using entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class JobController : ControllerBase
{
    private readonly IJobCommands _jobCommands;
    private readonly IJobQueries _jobQueries;

    private readonly ILogger<WorkerController> _logger;

    public JobController(ILogger<WorkerController> logger, IJobCommands jobCommands, IJobQueries jobQueries)
    {
        this._jobCommands = jobCommands;
        this._jobQueries = jobQueries;
        this._logger = logger;
    }

    [HttpGet]
    public async Task<IEnumerable<Jobs>> GetAsync()
    {
        var result = await _jobQueries.GetJobsAsync();
        return result;
    }

    [HttpGet("{jobId:int}")]
    public async Task<Jobs> GetByIdAsync([FromRoute] int jobId)
    {
        var result = await _jobQueries.GetJobsByIdAsync(jobId);
        return result;
    }

    [HttpGet("{jobType}")]
    public async Task<IEnumerable<Jobs>> GetByExpression([FromRoute] string jobType)
    {
        var result = await _jobQueries.GetJobsByExpression(o => o.JobTypeCatalogs.Name.Equals(jobType));
        return result;
    }

    [HttpPost]
    public async Task<bool> CreateAsync([FromBody] Jobs job)
    {
        var result = await _jobCommands.CreateAsync(job);
        return result;
    }

    [HttpPut]
    public async Task<bool> UpdateAsync([FromBody] Jobs job)
    {
        var result = await _jobCommands.UpdateAsync(job);
        return result;
    }

    [HttpDelete("{jobId:int}")]
    public async Task<bool> DeleteAsync([FromRoute] int jobId)
    {
        var result = await _jobCommands.DeleteAsync(jobId);
        return result;
    }
}
